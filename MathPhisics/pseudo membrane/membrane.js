// WebGL - Fundamentals
// from https://webglfundamentals.org/webgl/webgl-fundamentals.html
//"use strict";
// Vertex shader program
var VSHADER_SOURCE =
     
  'attribute vec4 a_Position;\n' +
  'uniform mat4 u_ViewMatrix;\n' +
    'uniform mat4 u_ProjMatrix;\n' + 
  'void main() {\n' +
  '  gl_Position = u_ProjMatrix * u_ViewMatrix*a_Position;\n'+
    '}\n';

// Fragment shader program
var FSHADER_SOURCE =
  'void main() {\n' +
  '  gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);\n' +
  '}\n';

var xx=0;
var r=3.14; 
var viewMatrix = new Matrix4(); 
function main() {
  // Retrieve <canvas> element
  var canvas = document.getElementById('c');

  // Get the rendering context for WebGL
  var gl = getWebGLContext(canvas);
  if (!gl) {
    console.log('Failed to get the rendering context for WebGL');
    return;
  }
var nf = document.getElementById('nearFar'); 
  // Initialize shaders
  if (!initShaders(gl, VSHADER_SOURCE, FSHADER_SOURCE)) {
    console.log('Failed to intialize shaders.');
    return;
  }
// Получить ссылку на переменную u_ProjMatrix
  var u_ProjMatrix = gl.getUniformLocation(gl.program,'u_ProjMatrix');

  // Создать матрицу для установки местоположения точки наблюдения
  var projMatrix = new Matrix4();
  // Write the positions of vertices to a vertex shader
  var n = initVertexBuffers(gl);
  if (n < 0) {
    console.log('Failed to set the positions of the vertices');
    return;
  }

  // Specify the color for clearing <canvas>
  gl.clearColor(0, 0, 0, 1);

  var tick = function() {
    draw(gl, n, u_ProjMatrix,
 projMatrix, nf);   // Draw function
    requestAnimationFrame(tick, canvas); // Request that the browser calls tick
  };
  tick();
}
function draw(gl, n, u_ProjMatrix,
 projMatrix, nf) {
  
  // Clear <canvas>
  gl.clear(gl.COLOR_BUFFER_BIT);
  initVertexBuffers(gl);
  var u_ViewMatrix = gl.getUniformLocation(gl.program,'u_ViewMatrix'); 

 

 viewMatrix.setLookAt(0.0, 1.0, 2.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0); 
 var g_near = -1, g_far = 3.0;
  var wh = 2.0;
  projMatrix.setOrtho(-wh, wh, -wh, wh, g_near, g_far); 
 
 gl.uniformMatrix4fv(u_ViewMatrix, false, viewMatrix.elements); gl.uniformMatrix4fv(u_ProjMatrix, false, projMatrix.elements);
  nf.innerHTML = 'near: ' + g_near + ', far: ' +
g_far;
  gl.drawArrays(gl.LINES, 0, n);
}
function fi(x)
{
    return Math.cos(6*x);
}
function ksi(x)
{
    return Math.cos(6*x);
}
function Dalamber(x,t)
{
    return 0.5*(fi(x-t) + fi(x+t))+0.5*(ksi(x+t)-ksi(x-t));
  
}
function initVertexBuffers(gl) {

  var vertices = new Float32Array(629*100);
  var i=0;
var ttt = - Math.PI;
  for(R=0.0;R<1.0;R+=0.01)
  {
    
    var tt = Dalamber(ttt,xx)/3;
  for(var t=-r;t<=r;t+=0.01)
  {
      vertices[i]=R*Math.cos(t);
      vertices[i+1]=tt;
      vertices[i+2]=R*Math.sin(t);
      i+=3;
  }
    ttt += Math.PI/100;
  }
  xx+=0.01;
  //if(xx>1)
    //xx=-1;
  var n = vertices.length/3;
  // Create a buffer object
  var vertexBuffer = gl.createBuffer();
  if (!vertexBuffer) {
    console.log('Failed to create the buffer object');
    return -1;
  }

  // Bind the buffer object to target
  gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
  // Write date into the buffer object
  gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);

  var a_Position = gl.getAttribLocation(gl.program, 'a_Position');
  if (a_Position < 0) {
    console.log('Failed to get the storage location of a_Position');
    return -1;
  }
  // Assign the buffer object to a_Position variable
  gl.vertexAttribPointer(a_Position, 3, gl.FLOAT, false, 0, 0);

  // Enable the assignment to a_Position variable
  gl.enableVertexAttribArray(a_Position);

  return n;
}
//main();
